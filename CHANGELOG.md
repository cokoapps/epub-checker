## [2.0.3](https://gitlab.coko.foundation/cokoapps/epub-checker/compare/v2.0.2...v2.0.3) (2025-02-04)


### Bug Fixes

* fix postgres ssl connections ([2dd77cb](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/2dd77cb05a28c63e0c2ef91016cb7489106ad683))

## [2.0.2](https://gitlab.coko.foundation/cokoapps/epub-checker/compare/v2.0.1...v2.0.2) (2024-12-13)


### Bug Fixes

* run create client from env script on startup ([d0deb48](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/d0deb483f0059d55466e6ddca5443c23e069b38c))

## [2.0.1](https://gitlab.coko.foundation/cokoapps/epub-checker/compare/v2.0.0...v2.0.1) (2024-12-10)


### Bug Fixes

* make sure java is installed on container ([b6b90b3](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/b6b90b3f99ddb59cf2f4364cd1e79b65da4cd9c2))

# [2.0.0](https://gitlab.coko.foundation/cokoapps/epub-checker/compare/v1.2.1...v2.0.0) (2024-12-06)


### chore

* upgrade to coko server v4 ([33500d9](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/33500d9a4ed284471987643680e926ecd6f592c9))


### BREAKING CHANGES

* environment variable changes: renamed PUBSWEET_SECRET to SECRET, dropped SERVER_PROTOCOL and
SERVER_HOST

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.1](https://gitlab.coko.foundation/cokoapps/epub-checker/compare/v1.1.0...v1.2.1) (2024-01-31)


### Bug Fixes

* typo in compose dev ([0a58996](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/0a58996eca0f20ff495a818f98158f99c5a379a3))
* **service:** fixes and node 16 ([32ec70a](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/32ec70afb4b474dd4cb25fa004e1ac153c8b1700))

## 1.1.0 (2021-04-16)


### Features

* **service:** auth endpoint added ([092fcda](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/092fcda31b84bec69458700a4ffd07533cf4ffb5))
* **service:** create client for development ([c61f1cb](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/c61f1cb7e4cb3bded29238075faab46e3611bced))
* **service:** dockerization ([40d1a93](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/40d1a93bedb312fd732db119947a5bca95374431))
* **service:** dockerization finalized ([48461a3](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/48461a3108dd78b6f3bd2ed84b54d89f7be672be))
* **service:** remove unnecessary commands from dockerfile ([9639fff](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/9639fffb4be2e6b00cd4e7e4ea5b268aa8b691c0))
* **service:** service dockerized and finalized ([578edd7](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/578edd797152732394ae5bb9e21d53f1cd7e10ea))
* **service:** use file server instead of http ([8e6d91b](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/8e6d91b0042790813015df3130278e7ff9467e71))


### Bug Fixes

* **service:** clean up tmp dir ([90fa191](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/90fa19125d6f5517aeac29e2ae2182349b88d162))
* **service:** docker ([2ff105c](https://gitlab.coko.foundation/cokoapps/epub-checker/commit/2ff105ce8f42dfdd9566745635735295c457ebb8))
